# Lessthan

Lessthan is an interpreter for a simple concatenative language, implemented in a number of programming languages as a learning exercise, using the [Readme Driven Development](https://tom.preston-werner.com/2010/08/23/readme-driven-development.html) model.

So far..

* ANSI C (C99)
* C++-17
* Clean 3.0
* ?

The interpreter reads a stream of characters one line at a time from standard input, evaluates each character in the context of a revolving stack, and prints the results to standard output - a classic REPL. Certain characters have special meaning: 

**Numbers** may be entered in unsigned integer format ` 42 `, unsigned real format ` 3.14 ` or (in some implementations, notably C++) unnsigned scientific format ` 1.0e-5 `. A number is terminated by a non-numeric character, including 'whitespace' characters, for example: ` 100 200+300- ` will evaluate to ` 0 `. Negative numbers are formed by applying the negate operator ` 99~ `. Internally all numbers are stored and manipulated as double precision real numbers. Boolean logic operators regard zero as false and any other value as true.

**Sequences** are consecutive characters enclosed in square braces. A sequence might represent a string ` [Hello, World!] ` or a stream of numbers and operators ` [6%1+*] `. Sequences may be arbitrarily nested ` [abc[def]ghi] ` but braces should be proerly matched if the contents are intended to be evaluated. Evaluating a sequence passes the contained sequence of characters to the interpreter. For example: ` [6%1+*]<, ` will print ` 42 ` to standard output. This feature is used to implement the branching operators 'if-else' and 'while'. For example: ` [[Yes],][[No],][1 2>]?, ` will print ` Yes ` to standard output.

**Operators** (outside of a sequence) are evaluated immediately in the context of the stack, which implies a postfix application. See below for the recognised operators. Operators may pop and/or push values on the stack. For example: ` 22 7 / , ` would consume the two numbers, perform the division and print: ` 3.14286 ` to standard output, leaving the stack empty. Some type checking is done on the values required by operators, but this is not exhaustive - there may be _undefined behavior_ if the stack does not hold the values expected by the operator.

**Characters** (not recognised as an operator) are pushed to the stack as an ascii char. 

**The stack** is a revolving stack of 42 slots, each of which can hold a number, a sequence or a single character value. The stack has a top and items are pushed and popped in last-in-first-out manner. Pushing the 43rd item will cause the top to revolve and the 1st item will be overwritten. Popping from an 'empty' stack will cause the top to reverse revolve and the 42nd item will be presented. Slots are immutable, other than the overwriting caused by exceeding the size of the stack. (((Implementation note - keep track of the bottom, so that printing the stack works?)))

**Slots** named by lowercase letters provide additional mutable storage for values outside the stack. For example: ` 13f: ` stores the number 13 to slot 'f' and ` g; ` retrieves the value from slot 'g'.

## Operators

The following operators are recognised and evaluated immediately by the interpreter:

| Operator | Name | Description |
| -------- | ---- | ----------- |
| % | Dup | Duplicate the item on top of stack |
| $ | Swap | Swap the top and next items |
| \\ | Drop | Drop the top item |
| ^ | Over | Copy the next item to the top |
| \" | Rot | Rotate the top item below the next two items |
| @ | Pick | Using number on top, copy the nth item to top |
||||
| + | Add | Add top to next |
| \- | Subtract | Subtract top from next |
| * | Multiply | Multiply next by top |
| / | Divide | Divide next by top |
| ~ | Negate | Negate top |
||||
| & | And | Logical and of next with top |
| \| | Or | Logical or of next with top |
| ! | Not | Logical not of top |
| = | Equals | True if next equals top, otherwise false |
| > | Greater | True if next is greater than top, otherwise false |
||||
| \: | Set | Set slot named by top to value in next |
| ;  | Get | Get value from slot named by top |
||||
| ? | If\-Else | If top is true evaluate next, otherwise evaluate below next |
| # | While | While top is true evaluate next | 
| < | Eval | Evaluate the sequence in top |
||||
| R | Read | Read contents of file named by top to a sequence |
| L | Literal | Push the following character as a literal |
| N | Newline | Emit newline character |
| , | Emit | Print top to standard output |
| S | Print | Print the stack to standard output |
| I | Intro | Print the file named 'intro.txt' to standard output |
| Q | Quit | Quit the interpreter |

The ` R ` operator effectively allows programs to be saved in files and read into the interpreter, for example: ` [myprog.lt]R<Q ` would read the contents of file 'myprog.lt' into a sequence on the top of stack, evaluate that sequence and quit. (((Implementation note - Consider command line options?)))

## Implementations

### Ansi-C

The first one! Working but not fully implemented..

Compile with: `gcc lessthan.c -lm -o ltansic`

### C++-17

Mostly implemented but some functionality not quite there..

Compile with: `g++ -std=c++17 lt.cpp -o ltcpp`

### Clean 3.0

A work in progress..

Compile with: `cpm make`  
(Hint: you will need to download and "install" clean-bundle-complete)

### Next..

I am considering Forth, a Lisp, Crystal, V-lang, Vala/Genie, reScript(?).. and would like to include Erlang/Elixir/LFE. My preference is for functional (or functional-capable) _compiled_ languages so forth and most Lisps probably will not make it. But considering the objective is to learn from a bunch of programming languages and improve my general programming ability, I should probably relax this preference. I am definitely NOT considering any of the 'popular' languages (Java, JS/Node, Python, etc.) but may include Ruby (although Crystal probably gets mostly there).

What about the ML family of languages? (Haskell, Scala, OCaml, SML, etc.) Well, Clean was an eye-opening discovery and I am enjoying learning this highly under-exposed language! At this stage I do not see the need to add any other of the (strictly) functional languages, although I have tried out Haskell, OCaml and MosML in the past and really enjoyed them.

## Possible extensions

Additional sequence manipulation operators might be considered:

| Operator | Name | Description |
| -------- | ---- | ----------- |
| A | Accumulate | Evaluate the binary sequence on top against each value in the sequence on next and the value below next, replacing the value below next with the result. |
| M | Map | Evaluate the binary sequence on top against each value in the sequence on next and the initial value below next, leaving a sequence of results on top |
| F | Filter | Evaluate the predicate sequence on top against each value in the sequence on next, leaving a sequence of values returning true |
| T | Top | Extract the first item from the sequence on top and push it to the stack, leaving the remainder of the sequence on next |
