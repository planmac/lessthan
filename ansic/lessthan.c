/* Lessthan
 * A terse, functional, stack-based interpreter
 * (c) planmac at gmail dot com 2020        
*/

/* TODO
 * How to create a sequence from code?
 * Implement other operators (foreach, filter, ?)
 * Operator to read a sequence from a file onto the stack?
 * Fix Termios echoing of chars
 * Optionally try to recover from errors
 * Implement the capital letter operators from original Lessthan
 * 
*/ 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <termios.h>

#include "lessthan.h"

//-------------------------------------------------------------------
// (Un)Comment this line to turn (on)off TERMIOS
//#define TERMIOS  

#ifdef TERMIOS
// Save current terminal settings and config for raw chars
#define TERMIOS_CONFIG                           \
  static struct termios oldTermios, newTermios;  \
  tcgetattr(STDIN_FILENO, &oldTermios);          \
  newTermios = oldTermios;                       \
  cfmakeraw(&newTermios);                        \
  tcsetattr(STDIN_FILENO, TCSANOW, &newTermios); 

// Restore terminal settings
#define TERMIOS_RESTORE tcsetattr(STDIN_FILENO, TCSANOW, &oldTermios);

// Putchar if using TERMIOS
#define TERMIOS_PUTCHAR(c) putchar(c);

#else
// This is best way to do noop!
#define TERMIOS_CONFIG do {} while (0);
#define TERMIOS_RESTORE do {} while (0);
#define TERMIOS_PUTCHAR(c) do {} while (0);

#endif //TERMIOS
//-------------------------------------------------------------------

/// Raise an error and exit the repl
void
lt_error(char *msg) {
  fprintf(stderr, "ERROR: %s\n\r", msg);
  exit(EXIT_FAILURE);
};

/// Read text file into a string
char*
lt_readtextfile(char *fname) {
  FILE *f = fopen(fname, "rb");
  if(f == NULL) {
    lt_error("File not found or not readable.");
    return NULL;
  };

  fseek(f, 0, SEEK_END);
  long fsize = ftell(f);
  fseek(f, 0, SEEK_SET);  /* same as rewind(f); */

  char *string = malloc(fsize + 1);
  if(string == NULL) {
    lt_error("Unable to reserve memory for the file.");
    return NULL;
  };

  fread(string, 1, fsize, f);
  fclose(f);

  string[fsize] = 0;
  return string;
};

/// String to double utility
double
lt_strtodbl(char *str) {
  char *eptr;
  long frc = 0;
  double res = 0;

  res = (double)strtol(str, &eptr, 10);
  // test errno?
  //if(errno == EINVAL || errno == ERANGE) {
  //  lt_error("Cannot parse number.");
  //  return 0; //??
  //}; 

  if(*eptr == '.') {
    frc = strtol(++eptr, NULL, 10);
    // test errno? (see above)
    res += frc / ((double) pow(10,strlen(eptr)));
  };

  return res;
};

/// The main parser for Lessthan!
/// Parse chars from file handle and push results onto stack.
/// Numbers and sequences are handed off to other functions for parsing.
void
lt_parsechar(Lt_stk *stk, FILE *fh) {
  char c;
  while( !feof(fh) ) {
    c = getc(fh); 

    if( (c >= '0' && c <= '9') || c == '.') {
      ungetc(c, fh); //Push back the first digit
      lt_parsenum(stk, fh);
      continue;
    };

    // ignore whitespace
    if(c <= ' ') {
      TERMIOS_PUTCHAR(' ')
      continue;
    };
  
    switch(c) {
      case 'Q':
        return;

      case 'S':
        printf("\n\r");
        stk_print(stk);
        printf("\n\r");
        break;

      case 'E':
        printf("%s", lt_readtextfile("examples.txt") );
        break;

      case 'R':
        stk_push(stk, mk_seq(lt_readtextfile((char*)stk_pop(stk))));
	//move this into a function
        break;

      case '[':
        lt_parseseq(stk, fh);
        break;

      case '%': op_dup(stk); break;
      case '$': op_swap(stk); break;
      case '\\': op_drop(stk); break;
      case '^': op_over(stk); break;
      case '\"': op_rot(stk); break;

      case '+': op_add(stk); break;
      case '-': op_subt(stk); break;
      case '*': op_mult(stk); break;
      case '/': op_dvid(stk); break;

      case '&': op_and(stk); break;
      case '|': op_or(stk); break;
      case '!': op_not(stk); break;
      case '=': op_equal(stk); break;
      case '>': op_gt(stk); break;

      case ':': op_set(stk); break;
      case ';': op_get(stk); break;
      case ',': op_emit(stk); break;
      case '?': op_ifelse(stk); break;
      case '#': op_while(stk); break;
      case '<': op_eval(stk); break;

      case 'N': stk_push(stk, mk_chr('\n')); op_emit(stk); break;
      case 'L': c = getc(fh); stk_push(stk, mk_chr(c)); break;
                
      default: stk_push(stk, mk_chr(c)); break;
    };


  TERMIOS_PUTCHAR(c)
  };
};

/// Parse contiguous digits and decimal point into a number type on stack,
/// and push the terminating non-numeric char back.
void
lt_parsenum(Lt_stk *stk,FILE *fh) {
  char *nbf = malloc(sizeof(char) * 80); 
  int n = 0;
  int flt = 0;
  char c = getc(fh);

  while( ((c >= '0') && (c <= '9')) || (c == '.') ) {
    if(c == '.') {
      if(flt) {
        lt_error("Invalid number format.");
        return;
      };

    flt = 1;
    };

    if(n >= 80) {
      nbf = realloc(nbf, sizeof(nbf) + sizeof(char) * 80);
    };

    nbf[n++] = c;
    TERMIOS_PUTCHAR(c)
    c = getc(fh);
  };

  stk_push(stk, mk_num(lt_strtodbl(nbf)) );
  ungetc(c, fh); //push back the first non-digit
  return; 
};

/// Parse a seq of chars (string) up to matching ']', and push onto stack
void
lt_parseseq(Lt_stk *stk, FILE *fh) {
  int sz = 80;
  char *cbf = malloc(sizeof(char) * 80); 
  int i = 0; 
  char c; 
  int b = 1; // bracket count, already 1

  while( (c = getc(fh)) ) {
    b += (c == '[') ? 1 : (c == ']') ? -1 : 0;
    if(b == 0) {break;};

    cbf[i++] = c;
    TERMIOS_PUTCHAR(c)

    if(i >= sz) {
      sz += 80;
      cbf = realloc(cbf, sizeof(cbf) + sizeof(char) * sz);
    };
  };

  cbf[i] = '\0';
  stk_push(stk, mk_seq(cbf));
  return; 
};

/// Push a string into the Lessthan char parser
void
lt_parsestring(Lt_stk *stk, char *str) {

  //Make string into a FILE*
  FILE *sh = tmpfile();
  if(sh == NULL) {
    lt_error("Could not create temp file.");
    return;
   };
  
  int nput = fputs(str, sh);
  if(nput <= 0) {
    lt_error("Could not write to temp file.");
    return;
  };

  fseek(sh, 0, SEEK_SET);  /* same as rewind(f); */
  lt_parsechar(stk, sh);

  fclose(sh);
  free(str);
  return;
};

/// Print any item type
void
lt_printitm(Lt_itm *itm) {
  if(itm == NULL) {
    printf("<NULL>");
    return;
  }

  switch(itm->typ) {

    case LT_NUM:
      printf("%f", itm->num);
      break;
    
    case LT_SEQ:
      printf("[%s]", itm->seq);
      break;

    case LT_CHR:
      printf("%c", itm->chr);
      break;
    
    default:
      printf("Print does not handle this type yet.\n\r");
  };
};

void
lt_repl() {
  Lt_stk *stk = stk_new();
    
  lt_parsechar(stk, stdin);

  free(stk);
  return;
};

// Stack functions ----------------------------------------- 

/// Make new stack
Lt_stk* 
stk_new() {
  Lt_stk *tmp = (Lt_stk*)malloc(sizeof(Lt_stk));
  if(tmp == NULL) {
    lt_error("Unable to create new stack.");
    return NULL;
  };

  stk_resize(tmp);
  return tmp;
};

/// Resize stack array to min/max free space of STK_CHUNK items
#define STK_CHUNK 10
void
stk_resize(Lt_stk *stk) {
  if(stk == NULL) {
    lt_error("Cannot resize a NULL stack.");
    return;
  };

  if( ((stk->len - stk->nxt) > STK_CHUNK) // Excess space
  ||  (stk->len == stk->nxt) // No space!
  ) {
    Lt_itm* *tmp; // Pointer to array of items
    tmp = (Lt_itm **)realloc(
      stk->itm, 
      sizeof(Lt_stk) * (stk->nxt + STK_CHUNK)
    );
    if(tmp == NULL) {
      lt_error("Unable to extend size of stack.");
      return;
    };
    stk->len = stk->nxt + STK_CHUNK;
    stk->itm = tmp;
  };
//printf("len: %lu, nxt: %lu\n\r", stk->len, stk->nxt);
  return;
};

void
stk_print(Lt_stk *stk) {
  int i;
  int len = stk->nxt;
  for(i=len -1; i >= 0; i--) {
    printf("%3d[%s] ", len-i, Typ[stk->itm[i]->typ]);
    lt_printitm(stk->itm[i]);
    printf("\n\r");
  };
  return;
};

/// Push an item onto a stack. 
void
stk_push(Lt_stk *stk, Lt_itm *itm) {
  stk_resize(stk);

  stk->itm[stk->nxt ++] = itm;
  //stk->itm[stk->nxt ++] = mk_dcpy(itm); //not sure if needed
  return;
};

/// Pop an item from a stack, and return a deep copy.
Lt_itm*
stk_pop(Lt_stk *stk) {
  if(stk->nxt == 0) {
    lt_error("You cannot pop from an empty stack." );
    return NULL;
  };

  //make a new item and resize the item array
  Lt_itm *tmp = mk_dcpy(stk->itm[-- stk->nxt]);
  stk_resize(stk);

  return tmp;
};

/// Return pointer to the top item on the stack.
Lt_itm*
stk_top(Lt_stk *stk) {
  if(stk->nxt == 0) {
    lt_error("Cannot top from an empty stack.");
    return NULL;
  };

  return stk->itm[stk->nxt -1];
};

// Type functions --------------------------------------------

/// Make new item of unspecified type
Lt_itm*
mk_itm() {
  Lt_itm *tmp = (Lt_itm*)malloc(sizeof(Lt_itm));
  if(tmp == NULL) {
    lt_error("Failed making new item.");
    return NULL;
  };
  return tmp;
};

/// Make a new number item, and initialise with a number
Lt_itm*
mk_num(double num) {
  Lt_itm *itm = mk_itm();
  itm->typ = LT_NUM;
  itm->num = (Lt_num)num;
  return itm;
};

/// Make a new sequence (string) item, and initialise with string
Lt_itm* 
mk_seq(char *str) {
  Lt_itm *itm = mk_itm();
  char *tmp = malloc(strlen(str) +1);
  strcpy(tmp, str);
  itm->typ = LT_SEQ;
  itm->seq = (Lt_seq)tmp;
  return itm;
};

// Make a new character item
Lt_itm*
mk_chr(char c) {
  Lt_itm *itm = mk_itm();
  itm->typ = LT_CHR;
  itm->chr = c;
  return itm;  
};

/// Deep copy item
Lt_itm*
mk_dcpy(Lt_itm *src) {
  Lt_itm *tmp;

  switch(src->typ) {
    case LT_NUM:
      tmp = mk_num(src->num);
      break;

    case LT_CHR:
      tmp = mk_chr(src->chr);
      break;

    case LT_SEQ:
      tmp = mk_seq(src->seq);
      break;
  };
  return tmp;
};

// Stack operators ----------------------------------------

/// Drop an item from a stack.
void
op_drop(Lt_stk *stk) {
  if(stk->nxt == 0) {
    lt_error("Cannot drop from an empty stack.");
    return;
  };

  stk->nxt --;
  stk_resize(stk);
  return;
};

/// Duplicate the top item of a sequence.
void
op_dup(Lt_stk *stk) {
  if(stk->nxt < 1) {
    lt_error("Cannot dup if less than 1 items on the stack.");
    return;
  };

  stk_push(stk, mk_dcpy(stk->itm[stk->nxt -1]));
};

/// Swap the top two items of a stack.
void
op_swap(Lt_stk *stk) {
  if(stk->nxt < 2) {
    lt_error("Cannot swap if less than 2 items on the stack.");
    return;
  };

  Lt_itm *tmp           = stk->itm[stk->nxt -1];
  stk->itm[stk->nxt -1] = stk->itm[stk->nxt -2];
  stk->itm[stk->nxt -2] = tmp;
  return;
};

/// Copy the item after top (ie next) to the top of a stack.
void
op_over(Lt_stk *stk) {
  if(stk->nxt < 2) {
    lt_error("Cannot over if less than 2 items on the stack.");
    return;
  };

  stk_push(stk, mk_dcpy(stk->itm[stk->nxt - 2]));
  return;
};

/// Rotate the top item below the item after next.
void
op_rot(Lt_stk *stk) {
  if(stk->nxt < 3) {
    lt_error("Cannot rot if less than 3 items on the stack.");
    return;
  };

  Lt_itm *tmp           = stk->itm[stk->nxt -1];
  stk->itm[stk->nxt -1] = stk->itm[stk->nxt -2];
  stk->itm[stk->nxt -2] = stk->itm[stk->nxt -3];
  stk->itm[stk->nxt -3] = tmp;
  return;
};

/// Pick (copy) the nth item from a stack to the top.
void
op_pick(Lt_stk *stk) {
  Lt_itm *nthitm = stk_pop(stk);
  if(nthitm->typ != LT_NUM) {
    lt_error("Pick requires a number type as index.");
    return;
  };

printf("num: %f fmod: %f", nthitm->num, fmod(nthitm->num,1));

  if(fmod(nthitm->num, 1) != 0) {
    lt_error("Cannot pick with a non-integer index.");
    return;
  };

  int nth = (int)nthitm->num;
  int len = stk->nxt;

  if(nth < 0 || (nth - 1) >= (len) ) {
    lt_error("Pick index out of range for this stack.");
    return;
  };

  stk_push(stk, mk_dcpy(stk->itm[len - nth]));
  return;
};

/// Emit - Print top of stack and drop
void
op_emit(Lt_stk *stk) {
  lt_printitm(stk_pop(stk));
  return;
};

/// Eval (apply) the operator on top of stack, 
/// possibly modifying the stack
void
op_eval(Lt_stk *stk) {
  Lt_itm *top = stk_pop(stk);

  //parse contents of the seq, char-by-char, onto the stack
  if(top->typ == LT_SEQ) {
    lt_parsestring(stk, top->seq);
    return;
  };

  // TODO Not sure what more needs to be done here?

  lt_error("Eval needs an operator or a sequence.");
  return;
};

/// Math operators -----------------------------------------------

#define MATHOPCHECK \
  if(stk->nxt < 2) { \
    lt_error("Math operator needs minimum 2 items on the stack."); \
    return; \
  }; \
  \
  Lt_itm *top = stk_pop(stk); \
  Lt_itm *nxt = stk_pop(stk); \
  \
  if(top->typ != LT_NUM || nxt->typ != LT_NUM) { \
    lt_error("Cannot apply math operator to non-numbers."); \
    return; \
  };
// enddefine MATHOPCHECK

/// Add top two numbers in the sequence, and push result
void
op_add(Lt_stk *stk) {
  MATHOPCHECK
  stk_push(stk, mk_num(nxt->num + top->num));
  return;
};

void
op_subt(Lt_stk *stk) {
  MATHOPCHECK
  stk_push(stk, mk_num(nxt->num - top->num));
  return;
};

void
op_mult(Lt_stk *stk) {
  MATHOPCHECK
  stk_push(stk, mk_num(nxt->num * top->num));
  return;
};

void
op_dvid(Lt_stk *stk) {
  MATHOPCHECK
  stk_push(stk, mk_num(nxt->num / top->num));
  return;
};

// Logic operators -------------------------------------------

// and or not equal gt ifelse
void
op_and(Lt_stk *stk) {
  MATHOPCHECK
  stk_push(stk, mk_num(nxt->num && top->num));
  return;
};

void
op_or(Lt_stk *stk) {
  MATHOPCHECK
  stk_push(stk, mk_num(nxt->num || top->num));
  return;
};

void
op_not(Lt_stk *stk) {
  if(stk->nxt < 1) {
    lt_error("Not operator needs minimum 1 items on the stack.");
    return;
  };
  
  Lt_itm *top = stk_pop(stk);
  
  if(top->typ != LT_NUM) {
    lt_error("Cannot apply math operator to non-numbers.");
    return;
  };
  stk_push(stk, mk_num(!(int)top->num));
  return;
};

void
op_equal(Lt_stk *stk) {
  MATHOPCHECK
  stk_push(stk, mk_num(nxt->num == top->num));
  return;
};

void
op_gt(Lt_stk *stk) {
  MATHOPCHECK
  stk_push(stk, mk_num(nxt->num > top->num));
  return;
};

void
op_ifelse(Lt_stk *stk) {
  if(stk->nxt < 3) { 
    lt_error("If-Else operator needs 3 items on the stack.");
    return;
  };
  
  op_rot(stk); //Put condition under the if/else branches
  Lt_itm *ifb = stk_pop(stk); //if-branch
  Lt_itm *elb = stk_pop(stk); //else-branch

  op_eval(stk); //Eval the condition
  Lt_itm *res = stk_pop(stk); //Pop the evaluated condition
  stk_push(stk, res->num ? ifb : elb ); //Push the relevant branch
  op_eval(stk); //Eval the branch

  return;
};

void
op_while(Lt_stk *stk) {
  if(stk->nxt < 2) { 
    lt_error("While operator needs 2 items on the stack.");
    return;
  };

  Lt_itm *cnd = stk_pop(stk); //condition
  Lt_itm *bdy = stk_pop(stk); //while-body

  //test condition first time
  stk_push(stk, cnd);
  op_eval(stk);
  int bool = stk_pop(stk)->num;

  while(bool) {
    //eval the body
    stk_push(stk, bdy); 
    op_eval(stk);
    
    //re-evel condition and loop
    stk_push(stk, cnd); 
    op_eval(stk); 
    bool = stk_pop(stk)->num;
  };

  return;
};

// Slot operators --------------------------------------------
// TODO Consider how to use chars other than only a..z as index

// set get
void
op_set(Lt_stk *stk) {
  Lt_itm *tgt = stk_pop(stk); //Char index
  if(tgt->chr <'a' || tgt->chr > 'z') {
    lt_error("Slot index out of range.");
    return;
  };
  Lt_itm *itm = stk_pop(stk);
  Slt[tgt->chr - 'a'] = itm;
  return;
};

// get
void
op_get(Lt_stk *stk) {
  Lt_itm *tgt = stk_pop(stk); //Char index
  if(tgt->chr <'a' || tgt->chr > 'z') {
    lt_error("Slot index out of range.");
    return;
  };
  
  stk_push(stk, Slt[tgt->chr - 'a']);
  return;
};

// Main /////////////////////////////////////////////////////
char *line1 = "Welcome to Lessthan ANSI-C";

char*
lt_spaced(char *s) {
  const size_t n = strlen(s);
  char *line1 = (char*)malloc(n*2 +3);
  int i;
  for (i = 0; i < n; i++) {
    line1[i*2] = s[i];
    line1[i*2 +1] = '_';
  };
  line1[i*2] = '\0';
  return line1;
};

char *intro = 
"% Dup  $ Swap  \\ Drop   ^ Over    \" Rot   \n\r"
"+ Add  - Subtr * Mult   / Divide  @ Pick    \n\r"
"& And  | Or    ! Not    = Equal   > Greater \n\r"
": Set  ; Get   @ Pick   ? If-Else # While   \n\r"
"N Newline      , Emit   < Eval    Q Quit!   \n\r"
"S Print the stack       R Read file to stack\n\r"
"L Literal next char     I Intro to Lessthan \n\r"
"\n\r";

int
main() {
  if (isatty(fileno(stdin))) {
    TERMIOS_CONFIG
    printf("%s\n\r%s", lt_spaced(line1), intro);

    lt_repl();

    printf("\n\rBye!\n\r");
    TERMIOS_RESTORE

  } else {
    lt_repl();
  };
  return 0;
}
