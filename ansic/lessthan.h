#ifndef LESSTHAN
#define LESSTHAN

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

/// A stack can hold these datatypes
typedef enum {
  LT_NUM,   // Number 
  LT_SEQ,   // Sequence
  LT_CHR,   // Character
} LT_TYP;

// Type names
char *Typ[] = {
  "Num",
  "Seq",
  "Chr"
};

/// Forward declarations, see below.
typedef struct LTSTK Lt_stk;
typedef struct LTITM Lt_itm;

/// Basic stack datatypes
typedef double  Lt_num;  // For now, to be expanded later
typedef char*   Lt_seq;  // Sequence is just a string
typedef char    Lt_chr;  // Char (index into slot array)

// A stack is an array with length and next indexes
struct LTSTK {
  size_t   len;   // Length of the array
  size_t   nxt;   // Next item to be used
  Lt_itm*  *itm;  // Array of pointers to items
};

// Composite item type
struct LTITM {
  LT_TYP    typ;
  union {
    Lt_num  num;
    Lt_seq  seq;
    Lt_chr  chr;
  };
};

// Array of 26 slots indexed by Slt[c - 'a']
Lt_itm *Slt[26];

////////////////////////////////////////////////////////

// General functions
void    lt_error(char*);
char*   lt_readtextfile(char*);
double  lt_strtodbl(char*);
char*   lt_spaced(char*);

void    lt_parsechar(Lt_stk*, FILE*);
void    lt_parsenum(Lt_stk*, FILE*);
void    lt_parseseq(Lt_stk *, FILE*);

void    lt_parsestring(Lt_stk*, char*);
void    lt_printitm(Lt_itm*);
void    lt_repl();

// Stack functions
Lt_stk* stk_new();
void    stk_resize(Lt_stk*);
void    stk_print(Lt_stk*);

void    stk_push(Lt_stk*, Lt_itm*);
Lt_itm* stk_pop(Lt_stk*);
Lt_itm* stk_top(Lt_stk*);

// Type functions
Lt_itm* mk_itm();
Lt_itm* mk_num(double);
Lt_itm* mk_seq(char*);
Lt_itm* mk_chr(char);
Lt_itm* mk_dcpy(Lt_itm *);


// Stack operators
void op_drop(Lt_stk*);
void op_dup(Lt_stk*);
void op_swap(Lt_stk*);
void op_over(Lt_stk*);
void op_rot(Lt_stk*);

void op_pick(Lt_stk*);
void op_emit(Lt_stk*);
void op_eval(Lt_stk*);

// Math operators
void op_add(Lt_stk*);
void op_subt(Lt_stk*);
void op_mult(Lt_stk*);
void op_dvid(Lt_stk*);

// Logic operators
void op_and(Lt_stk*);
void op_or(Lt_stk*);
void op_not(Lt_stk*);
void op_equal(Lt_stk*);
void op_gt(Lt_stk*); //greaterthan
void op_ifelse(Lt_stk*);
void op_while(Lt_stk*);

// Slot (variable) operators
void op_set(Lt_stk*);
void op_get(Lt_stk*);

#endif //LESSTHAN
