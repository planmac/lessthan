/* Lessthan
 * An interpreter for a simple concatenative language
 * implemented in 'modern' C++ (as a learning exercise!)
 */

/* TODO
  * Clean up parsing of chars in repl function:
 *   Multi-char test first, then single chars
 *   IO operators first, then non-IO operators
 * Revolving stack of 42 items ??
 * Sequence operators ..
 */

#include <iostream>
#include <string>
#include <stack>
#include <vector>
#include <variant>
#include <functional>
#include <stdexcept>
#include <set>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <unistd.h>

// Some utilities
// --------------

// helper type for the visitor overload
template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
// explicit deduction guide (not needed as of C++20)
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

// hack for boolean comparisons on strings
bool operator&&(std::string lhs, std::string rhs) {
  return lhs == rhs;
};
bool operator||(std::string lhs, std::string rhs) {
  return lhs != rhs;
};

// Derive from std::set to add contains() method.
class myset : std::set<char> {
  using set::set; //inherit constructors!
  public:
  bool contains(char k) {
    auto search = find(k);
    return search != end();
  };
};

// forward declaration
void repl(std::istream&);


// ----------------------------------------------------------
/// A stack of variant type items, either number (double), 
/// sequence (string) or operator (char). 
using Itm = std::variant<double, std::string, char>;
using Stk = std::stack<Itm, std::vector<Itm> >;


/// Stack class extending the Stk/std::stack class defined above.
class S : public Stk {

  public:
//======  
  //overwrite top and pop from std::stack to catch empty stack error
  Itm top() {
    if(size()>0) {
      const Itm n1 = Stk::top();
      return n1;
    };
    throw std::out_of_range("Stack is empty!");
  };

  Itm pop() {
    if(size()>0) {
      const Itm n1 = Stk::top();
      Stk::pop();
      return n1;
    };
    throw std::out_of_range("Stack is empty!");
  };

  // Stack operators
  void dup()  { push( top() ); };

  void drop() { pop(); };
  
  void swap() { // n2 n1 -- n1 n2 
    const Itm n1 = pop(); 
    const Itm n2 = pop();
    push(n1); 
    push(n2);
  };

  void rot()  { // n3 n2 n1 -- n1 n3 n2
    swap();
    const Itm n2 = pop();
    swap();
    push(n2);
  };
  
  void over() { // n2 n1 -- n2 n1 n2
    swap(); dup(); rot(); 
  };
  
  // How to tailcall optimise?
  Itm pickhelper(const int i) {
    if(i == 0) {
      return top();
    } else {
      const Itm nxt = pop();
      const Itm itt = pickhelper(i-1);
      push(nxt);
      return itt;
    };
  };
  
  void pick() { // nx .. n2 n1 -- nx .. n2 nx
    const Itm n1 = pop();
    if(n1.index() == 0) {
      int i = std::get<double>(n1) - 1;
      if(i < size()) { 
        push( pickhelper(i) );
      } else {
        throw std::out_of_range("Index must be <= stack size!");
      };

    } else {
      throw std::out_of_range("Index must be a number!");
    };
  };
  
  void negate() {
    const Itm n1 = pop();
    if(n1.index() == 0) {
      int v1 = std::get<double>(n1);
      push( v1 * -1.0);
    } else {
      throw std::invalid_argument("Cannot negate a non-number!");
    };
  };

  void nott() {
    const Itm n1 = pop();
    if(n1.index() == 0) {
      int v1 = std::get<double>(n1);
      push( v1 ? 0 : 1.0);
    } else {
      throw std::invalid_argument("Cannot negate a non-number!");
    };
  };

  void emit() { printitm(pop()); };

//----
  void set() {
    const Itm n1 = pop();
    if(auto pv1 = std::get_if<char>(&n1)) {
      slot[*pv1] = pop();
    } else {
      throw std::out_of_range("Reference for set must be a char.");
    };
  };

  void get() {
    const Itm n1 = pop();
    if(auto pv1 = std::get_if<char>(&n1)) {
      if(slot.count(*pv1) == 1) {
        push( slot.at(*pv1) );
      } else {
        throw std::out_of_range("Nothing in referenced slot!");
      };
    } else {
      throw std::out_of_range("Reference for get must be a char.");
    };
  };

  // Evaluate item on top of stack
  void eval() {
    const Itm n1 = top();
    switch(n1.index()) {
      case 0:  // Number and char eval to itself
      case 2: break;
      case 1: // Sequence must be parsed by repl()
        Stk::pop();
        std::istringstream iss(std::get<1>(n1));
        repl(iss);
        break;
    };
  };

  // [else_block] [if_block] [condition] -- [if-or-else]< 
  void ifelse() { 
    rot();
    const Itm ifb = pop();
    const Itm elb = pop();
    eval();
    const Itm res = pop();
    if(res.index() == 0) {
      auto r = std::get<0>(res);
      push( r ? ifb : elb);
      eval();
    } else {
      throw std::invalid_argument("ifelse condition must evaluate to a bool value.");
    };
  };

  void dowhile() { // [body] [condition] -- ??
    Itm res;
    
    const Itm cnd = top();
    swap();
    const Itm bdy = pop();
    eval(); 
    res = pop();

    while( res.index() == 0 and std::get<0>(res) != 0 ) {
      push(bdy);
      eval();
      push(cnd);
      eval();
      res = pop();
    };
  };

//----
  void applymop(char mop) { // n2 n1 -- (n2+n1)
    const Itm v1 = pop();
    const Itm v2 = pop();

    if( v2.index() == 0 && v1.index() == 0 ) {
      double n1 = std::get<0>(v1);
      double n2 = std::get<0>(v2);

      switch(mop) {
        case '+': push(n2 + n1); break;
        case '-': push(n2 - n1); break;
        case '*': push(n2 * n1); break;
        case '/': push(n2 / n1); break;

        default: 
          throw std::invalid_argument("Maths operator not recognised.");
      };
    } else {
      throw std::invalid_argument("Maths operator needs numbers.");
    };

  };

// More general solution for applying operators =============
// ???
// ==========================================================

  // Apply logical operators
  template<typename T>
  const Itm applylophelper(char lop, T i1, T i2) {
    switch(lop) {
      case '=': return (i2 == i1 ? 1.0 : 0); break;
      case '>': return (i2 > i1 ? 1.0 : 0); break;
      case '&': return (i2 && i1 ? 1.0 : 0); break;
      case '|': return (i2 || i1 ? 1.0 : 0); break;

      default: 
        throw std::invalid_argument("Logic operator nnot recognised.");
    };
  };

  void applylop(char lop) { // n2 n1 -- (n2==n1)
    const Itm v1 = pop();
    const Itm v2 = pop();

    if( v2.index() == v1.index() ) {
      switch(v1.index()) {
        case 0: 
          push( applylophelper(lop, std::get<0>(v1), std::get<0>(v2)) ); 
          break;
        case 1: 
          push( applylophelper(lop, std::get<1>(v1), std::get<1>(v2)) ); 
          break;
        case 2: 
          push( applylophelper(lop, std::get<2>(v1), std::get<2>(v2)) ); 
          break;
      };

    } else {
      throw std::invalid_argument("Logic operator needs same types.");
    };

  };


  void printstack() {
    std::size_t l = size() + 1;
    for(Stk tmp = *this; !tmp.empty(); tmp.pop()) {
      std::cout << "\nn" << (l - tmp.size()) << ": ";
      printitm(tmp.top());
    };
    std::cout << '\n';
  };

  void readfile(std::string fname) {
    const std::ifstream ifh(fname);
    std::stringstream tsh;
    tsh << ifh.rdbuf();
    push(tsh.str());
  };

  void newline() {
    std::cout << '\n';
  };

  private:
//=======  
  std::unordered_map<char, Itm> slot;

  void printitm(Itm itm) {
    std::visit( overloaded{
      [](double arg) {std::cout << arg;},
      [](std::string arg) {std::cout << '[' << arg << ']';},
      [](char arg) {std::cout << "'" << arg << "'";},
      
    }, itm);
  };

  void printitm2(Itm itm) {
    switch(itm.index()) {
      case 0: std::cout << std::get_if<0>(&itm); break;
      case 1: std::cout << '[' << std::get_if<1>(&itm) << ']'; break;
      case 2: std::cout << "'" << std::get_if<2>(&itm) << "'"; break;
    };
  };

};


// Read from standard input, parse chars for i) number, 
// ii) [sequence], or iii) char operator, and push the parsed 
// value onto a stack. operators are evaluated as encountered, 
// possibly altering the stack. Exit the repl with 'Q'.
void repl(std::istream& chin) {
  static S stk;
  double n; std::string s; char c;

  myset maths_ops = {'+','-','*','/'};
  myset logic_ops = {'&','|','=','>'};
  //myset stack_ops = {'%','$','\\','^,'"',  '@'};
  //myset other_ops = {':',';',',','?','#',  '!','~'};

  chin.unsetf(std::ios_base::skipws);

  while( chin >> c) {
    if(c == 'Q') break;

    if(c <= ' ') continue;

    if(c == 'N') {stk.newline(); continue;};

    if(c == 'S') {stk.printstack(); continue;};

    if(c == 'R') {
      stk.readfile( std::get<std::string>(stk.pop()) ); 
      // Safety checks??
      continue;
    };

    if(c == 'I') {
      stk.readfile("intro.txt"); 
      stk.eval();
      continue;
    };

    if(c == 'L') {
      chin >> c;
      stk.push(c);
      continue;
    };

    // Number starts with a digit.
    if ( (c >= '0') && (c <= '9') ) {
      chin.putback (c); // Put back the first digit.
      chin >> n;
      stk.push(n);
      continue;
    };

    // Sequence is bounded by [ .. ] and can be nested.
    if ( c == '[' ) {
      std::string s;
      int b = 1;
      chin >> c;
      do { 
        b += (c == '[') ? 1 : (c == ']') ? -1 : 0;
        if(b > 0) {
          s.push_back(c);
          chin >> c;
        }; 
      } while(b > 0);
      stk.push(s);
      continue;
    };

    if ( maths_ops.contains(c) ) {
      stk.applymop(c);
      continue;
    };

    if ( logic_ops.contains(c) ) {
      stk.applylop(c);
      continue;
    };

    switch(c) {
      case '%': stk.dup(); break;
      case '$': stk.swap(); break;
      case '\\': stk.drop(); break;
      case '^': stk.over(); break;
      case '"': stk.rot(); break;
      case '@': stk.pick(); break;

      case ':': stk.set(); break;
      case ';': stk.get(); break;
      case ',': stk.emit(); break;
      case '?': stk.ifelse(); break;
      case '#': stk.dowhile(); break;

      case '!': stk.nott(); break;
      case '~': stk.negate(); break;


      case '<': stk.eval(); break;

      default: stk.push(c);
    };

  };

  //stk.printstack(); // For testing

  return;  
};

/// Main =====================================================
int main() {

  if (isatty(fileno(stdin))) {
    std::string s {"Welcome to Lessthan C++"};
    for(auto c : s) std::cout << c << '_';
    std::cout << "\n\r"
    <<  "% Dup  $ Swap  \\ Drop   ^ Over    \" Rot\n\r"
    <<  "+ Add  - Subtr * Mult   / Divide  ~ Negate\n\r"
    <<  "& And  | Or    ! Not    = Equal   > Greater\n\r"
    <<  ": Set  ; Get   @ Pick   ? If-Else # While\n\r"
    <<  "N Newline      , Emit   < Eval    Q Quit!\n\r"
    <<  "S Print the stack       R Read file to stack\n\r"
    <<  "L Literal next char     I Intro to Lessthan\n\r"
    <<  "\n\r";
  };

  try {
    repl(std::cin);
  } catch(const std::exception& e) {
    std::cerr << e.what();
  };

  if (isatty(fileno(stdin))) {
    std::cout << "\nBye!\n";
  };

  return 0;
}
