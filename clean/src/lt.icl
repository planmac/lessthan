/* Lessthan
 * An interpreter for a simple concatenative language
 * implemented in Clean <clean.cs.ru.nl> (as a learning exercise!)
 */

/* TODO
 * Lots!!!
 */

module lt

import StdEnv
import System.CommandLine
import System.OS
import StdMaybe
import Data.CircularStack
import Text.StringAppender
import qualified Data.Map // Seems to conflict with another import

// ------------------------------------------------------------------
// Data types

// An algebraic data type of items that can be either  
// number (Real), sequence (String) or character (Char). 
::Itm = Num Real | Seq String | Chr Char

// A circular stack of Itm items
::Stk :== CircularStack Itm

// Slots for mutable storage of Itm items
::Slt :== 'Data.Map'.Map Char Itm

// State tuple type, for passing the stack and the slots around! 
::Stt :== (Stk,Slt)

// ------------------------------------------------------------------
// Stack manipulation functions

ltdup :: Stt -> Stt
ltdup (stk,slt) 
# itm = fromJust (peek stk)
  stk = push itm stk
= (stk,slt)

ltswap :: Stt -> Stt
ltswap (stk,slt)
# (top,stk) = pop stk
  top = fromJust top
  (nxt,stk) = pop stk
  nxt = fromJust nxt
  stk = push top stk
  stk = push nxt stk
= (stk,slt)

ltdrop :: Stt -> Stt
ltdrop (stk,slt)
# (_,stk) = pop stk
= (stk,slt)

ltover :: Stt -> Stt
ltover (stk,slt)
# (top,stk) = pop stk
  top = fromJust top
  nxt = fromJust (peek stk)
  stk = push top stk
  stk = push nxt stk
= (stk,slt)

ltrot :: Stt -> Stt
ltrot (stk,slt)
# (top,stk) = pop stk
  top = fromJust top
  (nxt,stk) = pop stk
  nxt = fromJust nxt
  (trd,stk) = pop stk
  trd = fromJust trd
  stk = push top stk
  stk = push trd stk
  stk = push nxt stk
= (stk,slt)

ltadd :: Stt -> Stt
ltadd (stk,slt)
# (top,stk) = pop stk
  (Num top) = fromJust top
  (nxt,stk) = pop stk
  (Num nxt) = fromJust nxt
  stk = push (Num (nxt + top)) stk
= (stk,slt)

ltsubtr :: Stt -> Stt
ltsubtr (stk,slt)
# (top,stk) = pop stk
  (Num top) = fromJust top
  (nxt,stk) = pop stk
  (Num nxt) = fromJust nxt
  stk = push (Num (nxt - top)) stk
= (stk,slt)

ltmult :: Stt -> Stt
ltmult (stk,slt)
# (top,stk) = pop stk
  (Num top) = fromJust top
  (nxt,stk) = pop stk
  (Num nxt) = fromJust nxt
  stk = push (Num (nxt * top)) stk
= (stk,slt)

ltdiv :: Stt -> Stt
ltdiv (stk,slt)
# (top,stk) = pop stk
  (Num top) = fromJust top
  (nxt,stk) = pop stk
  (Num nxt) = fromJust nxt
  stk = push (Num (nxt / top)) stk
= (stk,slt)

ltneg :: Stt -> Stt
ltneg (stk,slt)
# (top,stk) = pop stk
  (Num top) = fromJust top
  stk = push (Num (0.0 - top)) stk
= (stk,slt)

ltand :: Stt -> Stt
ltand (stk,slt)
# (top,stk) = pop stk
  (Num top) = fromJust top
  (nxt,stk) = pop stk
  (Num nxt) = fromJust nxt
| nxt <> 0.0 && top <> 0.0 = (push (Num 1.0) stk,slt)
= (push (Num 0.0) stk,slt)

ltor :: Stt -> Stt
ltor (stk,slt)
# (top,stk) = pop stk
  (Num top) = fromJust top
  (nxt,stk) = pop stk
  (Num nxt) = fromJust nxt
| nxt <> 0.0 || top <> 0.0 = (push (Num 1.0) stk,slt)
= (push (Num 0.0) stk,slt)

ltnot :: Stt -> Stt
ltnot (stk,slt)
# (top,stk) = pop stk
  (Num top) = fromJust top
| top == 0.0 = (push (Num 1.0) stk,slt)
= (push (Num 0.0) stk,slt)

//For Num types only
ltequal :: Stt -> Stt
ltequal (stk,slt)
# (top,stk) = pop stk
  (Num top) = fromJust top
  (nxt,stk) = pop stk
  (Num nxt) = fromJust nxt
| nxt == top = (push (Num 1.0) stk,slt)
= (push (Num 0.0) stk,slt)

ltgreater :: Stt -> Stt
ltgreater (stk,slt)
# (top,stk) = pop stk
  (Num top) = fromJust top
  (nxt,stk) = pop stk
  (Num nxt) = fromJust nxt
| nxt > top = (push (Num 1.0) stk,slt)
= (push (Num 0.0) stk,slt)

ltset :: Stt -> Stt
ltset (stk,slt)
# (k,stk) = pop stk
  (Chr k) = fromJust k
  (v,stk) = pop stk
  v = fromJust v
  slt = 'Data.Map'.put k v slt
= (stk,slt)

ltget :: Stt -> Stt
ltget (stk,slt)
# (k,stk) = pop stk
  (Chr k) = fromJust k
  r = 'Data.Map'.get k slt
  v = case r of 
        ?Just v = v
        otherwise = abort "Attempt to get unset key."
= (push v stk,slt)

ltpick :: Stt -> Stt
ltpick (stk,slt)
# (n,stk) = pop stk
  (Num n) = fromJust n
  n = toInt n - 1
  tmp = newStack 42
  (nth,stk,tmp) = getnth n stk tmp
  stk = pushback n stk tmp
= (push nth stk,slt)
where
  getnth n stk tmp
  | n == 0 = (fromJust (peek stk),stk,tmp)
  # (top,stk) = pop stk
    top = fromJust top
    tmp = push top tmp
  = getnth (n-1) stk tmp
  //
  pushback n stk tmp
  | n == 0 = stk
  # (top,tmp) = pop tmp
    top = fromJust top
    stk = push top stk
  = pushback (n-1) stk tmp

//ifelse stk
//ltwhile stk

ltnewline :: Stt *File -> (Stt,*File)
ltnewline (stk,slt) fh
# stk = push (Chr '\n') stk
= ltemit (stk,slt) fh

//lteval stk

//readfile stk fh
//ltintro stk fh

// ------------------------------------------------------------------
// IO  functions

writeitm :: Itm *File -> *File
writeitm itm fh
# s = case itm of
        Num n = toString n
        Seq s = s
        Chr c = toString c
= fwrites s fh

printstk :: Stt *File -> (Stt,*File)
printstk (stk,slt) fh 
# lst = toList stk
  ls2 = [ (i,t) \\ i <- [1..] & t <- lst ]
  fh = fwritec '\n' fh
  fh = foldl (\ fh itm -> printitm itm fh) fh ls2
  fh = fwritec '\n' fh
= repl (stk,slt) fh
where
  printitm (i,t) fh
  # fh = fwrites ((toString i) +++ ": ") fh
    fh = writeitm t fh
  = fwritec '\n' fh

ltemit :: Stt *File -> (Stt,*File)
ltemit (stk,slt) fh
# (top,stk) = pop stk
  itm = case top of 
    ?Just a = a
    ?None = abort "Unable to emit top of stack."
  fh = writeitm itm fh
= repl (stk,slt) fh

literal :: Stt *File -> (Stt,*File)
literal (stk,slt) fh
# (c,fh) = getchar fh
  stk = push (Chr c) stk
= repl (stk,slt) fh

newline :: Stt *File -> (Stt,*File)
newline (stk,slt) fh
# stk = push (Chr '\n') stk
= ltemit (stk,slt) fh

// ------------------------------------------------------------------
// Parsing numbers, sequences and characters

parsenum :: Stt {#Char} *File -> (Stt,*File)
parsenum (stk,slt) pnum fh
# (c,fh) = getchar fh
| (c >= '0' && c <= '9') = parsenum (stk,slt) (pnum +++ (toString c)) fh
| c == '.' = parsenum (stk,slt) (pnum +++ (toString c)) fh
| pnum <> toString (toReal pnum) = abort ("Illegal number format: " +++ pnum)
# itm = Num (toReal pnum)
  stk = push itm stk
//  fh = fwrites ("pnum: " +++ pnum +++ "\n") fh
= parsechr (stk,slt) c fh

parseseq :: Stt {#Char} Int *File -> (Stt,*File)
parseseq (stk,slt) pseq bcnt fh
# (c,fh) = getchar fh
  bcnt = incbcnt bcnt c
| bcnt == 0 
  # itm = Seq pseq
    stk = push itm stk
//    fh = fwrites ("pseq: " +++ pseq +++ "\n") fh
  = repl (stk,slt) fh
= parseseq (stk,slt) (pseq +++ toString c) bcnt fh
where
  incbcnt bcnt c
  | c == '[' = bcnt + 1
  | c == ']' = bcnt - 1
  = bcnt

parsechr :: Stt Char *File -> (Stt,*File)
parsechr (stk,slt) c fh 
| c == 'Q' = ((stk,slt),fh)  //abort "Bye!\n"
| c <= ' ' = repl (stk,slt) fh
| (c >='0' && c <= '9') = parsenum (stk,slt) {c} fh
| c == '[' = parseseq (stk,slt) {} 1 fh
| c == ',' = ltemit (stk,slt) fh
| c == 'S' = printstk (stk,slt) fh
| c == 'L' = literal (stk,slt) fh
| c == 'N' = newline (stk,slt) fh
# (stk,slt) = case c of
        '%' = ltdup (stk,slt)
        '$' = ltswap (stk,slt)
        '\\' = ltdrop (stk,slt)
        '^' = ltover (stk,slt)
        '"' = ltrot (stk,slt)
        '+' = ltadd (stk,slt)
        '-' = ltsubtr (stk,slt)
        '*' = ltmult (stk,slt)
        '/' = ltdiv (stk,slt)
        '~' = ltneg (stk,slt)
        '&' = ltand (stk,slt)
        '|' = ltor (stk,slt)
        '!' = ltnot (stk,slt)
        '=' = ltequal (stk,slt)
        '>' = ltgreater (stk,slt)
        ':' = ltset (stk,slt)
        ';' = ltget (stk,slt)
        '@' = ltpick (stk,slt)
        otherwise = (push (Chr c) stk,slt)
= repl (stk,slt) fh

getchar :: *File -> (Char,*File)
getchar fh
# (ok,c,fh) = freadc fh
| not ok = abort "Unable to read char from console."
= (c,fh)

// ------------------------------------------------------------------
// REPL and Start entry point

repl :: Stt *File -> (Stt,*File)
repl stt fh
# (c,fh) = getchar fh
= parsechr stt c fh

list1 :: [Char]
list1 =: fromString "Welcome to Lessthan <lean"
line1 =: toString (joinList "_" list1 newAppender)

intro =: line1 +++ "\n" 
  +++ "% Dup  $ Swap  \\ Drop   ' Over    \" Rot   \n"
  +++ "+ Add  - Subtr * Mult   / Divide  ~ Negate  \n"
  +++ "& And  | Or    ! Not    = Equal   > Greater \n"
  +++ ": Set  ; Get   @ Pick   ? If-Else # While   \n"
  +++ "N Newline      , emit   < Eval    Q Quit!   \n"
  +++ "S Print the stack       R Read file to stack\n"
  +++ "L Literal next char     I Intro to Lessthan \n"
  +++ "\n"

Start :: *World -> Int //*World
Start world
# (console,world) = stdio world
  (res,world) = isatty console world
  //console = fwrites ("res: " +++ (toString res)) console
  console = if (res == 1) (fwrites intro console) console
  (stt,console) = repl ( (newStack 42), ('Data.Map'.newMap) ) console
  //console = fwrites ("res2: " +++ (toString res)) console
  console = if (res == 1) (fwrites "Bye!\n" console) console
  (ok,world) = fclose console world
//= setReturnCode (if ok 0 1) world
| not ok = 1
= 0
where
  isatty fd w = IF_MAC (isattyC fd w) (0, w)
//  isattyC :: !Int !*w -> (!Int, !*w)
  isattyC :: *File !*w -> (!Int, !*w)
  isattyC _ _ = code {
    ccall isatty "I:I:A"
  }

