//! Lessthan
//! A terse, functional, stack-based interpreter
//! implemented in zig as a learning exercise
//! (c) planmac at gmail dot com 2020
//!

// Info on how to use stdin and stdout here:
// https://stackoverflow.com/questions/62018241/current-way-to-get-user-input-in-zig

const std = @import("std");
const stdin = std.io.getStdIn().reader();
const stdout = std.io.getStdOut().writer();

/// Stack of Items: Items may be numbers, sequences (strings) or chars.
/// The stack is a linked list of items with the 'top-of-stack' being the
/// first item. This implies that pushing and popping requires explicit
/// memory allocation / deallocation!
const Itm = union(enum) {
    num: f128, //too big??
    seq: []const u8, //string is a utf-8 encoded slice of u8
    chr: u8,
};

const Stk = struct {
    itm: Itm,
    nxt: ?*Itm,
};

pub fn makeChr(c: u8) Itm {
    const tmp = Itm{ .chr = c };
    return tmp;
}

/// The stack is a struct type named S
const S = struct {
    top: *Stk = undefined,

    // Stack functions
    pub fn stkPush(self: S, itm: Itm) void {
        const new = Stk{ .itm = itm, .nxt = &self.top.itm };
        self.top = &new;
    }

    pub fn stkPop(self: S) Itm {
        const old = self.top;
        const top = old.itm;
        const nxt = old.nxt;
        self.top = nxt;
        //free old ??
        return top;
    }

    // Operators
    pub fn opDup(self: S) void {
        const tmp = self.top.itm;
        stkPush(tmp);
    }

    pub fn opDrop(self: S) void {
        _ = stkPop();
    }

    pub fn opSwap(self: S) void {
        const top = self.top.itm;
        const nxt = self.top.nxt;
        self.top.nxt = &top;
        self.top.itm = nxt;
    }
}; // end S

/// State of the repl
const State = union(enum) {
    interp: void,
    number: void,
    sequence: void,
    literal: void,
    quit: u8,
};

/// Read, eval, print, loop
pub fn repl() !void {
    var stk = S{ .top = undefined };

    //undefined; // The stack for this repl
    var state = State.interp; // Current state of the repl
    var c: u8 = undefined; // Current character
    var nb: u8 = 0; // Number of [braces] in the current sequence
    var base: f128 = 10; // Current number base

    while (state != State.quit) : (c = try stdin.readByte()) {
        //c = try stdin.readByte();

        // Accumulate a temp sequence
        if (state == State.sequence) {
            if (c == @intCast(u8, '[')) {
                nb += 1;
            } else if (c == @intCast(u8, ']')) {
                nb -= 1;
                if (nb == 0) {
                    //push temp sequence onto stack;
                    state = State.interp;
                    continue;
                }
            }
            //append c to temp sequence;
            continue;
        } //end-build-seq

        if (state == State.literal) {
            stk.stkPush(makeChr(c));
            continue;
        }

        // Ignore whitespace
        if (c <= @intCast(u8, ' ')) {
            continue;
        }

        // Process digits
        if ((c >= '0') and (c <= '9')) {
            switch (state) {
                .interp => {
                    const tmp = Itm{ .num = @intToFloat(f128, c - '0') };
                    stk.stkPush(tmp);
                    state = State.number;
                },

                .number => {
                    stk.top.itm.num = stk.top.itm.num * base + @intToFloat(f128, c - '0');
                },

                .sequence => {
                    //do nothing, the digit char will be pushed on the stack by the _ case
                },

                .literal => unreachable,
                .quit => unreachable,
            } //end-switch-on-state
        } //end-digits

        switch (c) {
            //'Q' => state = State{ .quit = 0 },
            'L' => state = State.literal,

            '[' => {
                state = State.sequence;
                nb += 1;
            },

            '%' => stk.opDup(),
            '\\' => stk.opDrop(),
            '$' => stk.opSwap(),

            //more cases on which to dispatch

            // Default case
            else => {
                // stkPush( makeChr(c))
            },
        } //end-switch-on-c
    } //end-while

    //return state.quit; //would be useful to return the value in .quit
}

//    var buf: [1024]u8 = undefined;
//    if (try stdin.readUntilDelimiterOrEof(buf[0..], '\n')) |line| {
//        try stdout.print("Got: {}\n", .{line});
//    }

// ------------------------------------------------------------------
pub fn main() !void {
    const isatty = std.io.getStdIn().isTty();

    if (isatty) {
        try stdout.print("Welcome to Lessthan ANSI-C\n", .{});
    }

    try repl();

    if (isatty) {
        try stdout.print("Bye!\n", .{});
    }
}
