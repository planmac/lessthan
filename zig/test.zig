const std = @import("std");

const State = union(enum) {
    a: u8,
    b,
    c: void,
};

pub fn main() void {
    var s: State = undefined;

    s = State.b;
    //s = State{ .a = 42 };

    while (s != State.a) {
        switch (s) {
            .a => |n| std.debug.print("a: {}\n", .{n}),
            .b => std.debug.print("b!\n", .{}),
            .c => std.debug.print("c!\n", .{}),
        }
    }

    var c: u8 = 'a';
    std.debug.print("c is {c} and {d}\n", .{ c, c });
    if (c >= '0' and c <= '9') {
        std.debug.print("yes\n", .{});
    } else {
        std.debug.print("no\n", .{});
    }
}
